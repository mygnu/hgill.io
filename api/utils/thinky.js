'use strict'
// Import
const path = require('path')
const config = require(path.join(__dirname, '/config'))
const thinky = require('thinky')(config)

module.exports = thinky
