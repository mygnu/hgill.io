'use strict'
const path = require('path')
const shortid = require('shortid')
const handleError = require(path.join(__dirname, '/utils/handleError'))
const handleSucess = require(path.join(__dirname, '/utils/handleSucess'))
const Relation = require(path.join(__dirname, '/models')).Relation

exports.getAll = function (req, res) {
    Relation.run()
        .then(function (edges) {
            res.json(edges)
        })
        .error(handleError(res))
}

exports.getById = function (req, res) {
    var id = req.params.id
    Relation.get(id).run()
        .then(function (edge) {
            res.json(edge)
        })
        .error(handleError(res))
}

exports.add = function (req, res) {
    const data = req.body
    console.log(data)
    if (data instanceof Array) {
        for (const rel of data) {
            rel.id = rel.id || shortid.generate()
            const relation = new Relation(data)
            relation.save().then(function (result) {
                    // handleSucess(res, result)
                })
                // .error(handleError(res))
        }
    } else {
        // generate a short url friendly id
        data.id = data.id || shortid.generate()
        const relation = new Relation(data)
        relation.save().then(function (result) {
                handleSucess(res, result)
            })
            .error(handleError(res))
    }
}

exports.delete = function (req, res) {
    const data = req.body
    console.log(data)
    if (data instanceof Array) {
        for (const id of data) {
            Relation.get(id).then(function (relation) {
                relation.delete()
            })
        }
    } else {
        Relation.get(data).then(function (relation) {
            relation.delete()
        })
    }
}
