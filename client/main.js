'use strict'
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueMdl from 'vue-mdl'
import Home from './components/Home.vue'
import App from './components/App.vue'
import Works from './components/Works.vue'
import Maporg from './components/maporg/Maporg.vue'

// ...
Vue.use(VueResource)
Vue.use(VueRouter)
Vue.use(VueMdl)

Vue.config.debug = true
var router = new VueRouter({
    // history: true,
    hashbang: true,
    linkActiveClass: 'active'
})

// Set up routing and match routes to components
router.map({
    '/': {
        component: Home
    },
    '/works': {
        component: Works
    },
    '/maporg': {
        component: Maporg
    }
})

// Redirect to the home route if any routes are unmatched
router.redirect({
        '*': '/'
    })
    // Start the app on the #app div
router.start(App, 'div#app')
